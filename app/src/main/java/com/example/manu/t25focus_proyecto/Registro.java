package com.example.manu.t25focus_proyecto;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Registro extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private UserViewModel mUserViewModel;
    public static int TEXT_REQUEST = 0;
    private EditText usuario,email,pwd,nombre,apellidos,fecha;
    private Spinner sexo;
    private Button reiniciar,enviar;
    List<User> usuarios = new ArrayList<User>();
    ArrayAdapter<CharSequence> adapter;
    private ImageView camara;
    final int CODI_CAMERA = 1;
    private static final int RECOVERY_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        usuarios = MainActivity.usuarios;
        usuario = findViewById(R.id.etxtUsuario);
        email = findViewById(R.id.etxtEmail);
        pwd = findViewById(R.id.etxtPassword);
        nombre = findViewById(R.id.etxtNombre);
        apellidos = findViewById(R.id.etxtApellidos);
        fecha = findViewById(R.id.etxtFecha);
        sexo = findViewById(R.id.spnrSexo);
        reiniciar = findViewById(R.id.btnReiniciar);
        enviar = findViewById(R.id.btnEnviar);
        camara = findViewById(R.id.imgRegistro);
        adapter = ArrayAdapter.createFromResource(this,R.array.sexo,R.layout.support_simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        sexo.setAdapter(adapter);
        sexo.setOnItemSelectedListener(this);
        if(TEXT_REQUEST == 1){
            usuario.setText(getIntent().getExtras().getStringArray("usuario")[0]);
            email.setText(getIntent().getExtras().getStringArray("usuario")[1]);
            pwd.setText(getIntent().getExtras().getStringArray("usuario")[2]);
            nombre.setText(getIntent().getExtras().getStringArray("usuario")[3]);
            apellidos.setText(getIntent().getExtras().getStringArray("usuario")[4]);
            sexo.setSelection(Integer.parseInt(getIntent().getExtras().getStringArray("usuario")[5]));
            fecha.setText(getIntent().getExtras().getStringArray("usuario")[6]);
            usuario.setEnabled(false);
            pwd.setEnabled(false);
            reiniciar.setEnabled(false);
            enviar.setText("Modificar");


        }
        mUserViewModel = ViewModelProviders.of(this).get(UserViewModel.class);

        camara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(i, CODI_CAMERA);
            }
        });

    }

    public void efectuarRegistro(View view) {
        Boolean repetido = false;
        User cuenta = new User (usuario.getText().toString(),email.getText().toString(),pwd.getText().toString()
                ,nombre.getText().toString(),apellidos.getText().toString(),
                Integer.toString(sexo.getSelectedItemPosition()),fecha.getText().toString());
        for (int i = 0; i < usuarios.size(); i++){
            if (usuarios.get(i).getMNombreUser().equals(cuenta.getMNombreUser())) {
                if(TEXT_REQUEST == 1){
                    mUserViewModel.update(cuenta);
                    Menu.usuario = new String[]{cuenta.getMNombreUser(), cuenta.getMCorreo(),
                            cuenta.getMPassword(), cuenta.getMNombre(), cuenta.getMApellido(),
                            cuenta.getMSexo(), cuenta.getMFNacimiento(),String.valueOf(usuarios.get(i).getPeso()),
                            String.valueOf(usuarios.get(i).getBrazo()),String.valueOf(usuarios.get(i).getPecho()),
                            String.valueOf(usuarios.get(i).getCintura()),String.valueOf(usuarios.get(i).getPiernas())};
                    Toast.makeText(this,getString(R.string.perfilModificado), Toast.LENGTH_LONG).show();
                    finish();
                    break;

                }
                else{
                    Toast.makeText(this, getString(R.string.repetido), Toast.LENGTH_LONG).show();
                    usuario.setText("");
                    repetido = true;
                    break;
                }

            }
            else if(cuenta.getMNombreUser().equals("")||cuenta.getMPassword().equals("")){
                repetido = true;
                Toast.makeText(this, getString(R.string.requisitos), Toast.LENGTH_LONG).show();
                break;
            }
            repetido = false;
        }
        if(!repetido && TEXT_REQUEST == 0){
            mUserViewModel.insert(cuenta);
            Intent intent = new Intent(this, MainActivity.class);
            //intent.putExtra("usuarios",usuarios);
            startActivity(intent);
            MainActivity.TEXT_REQUEST = 1;
            finish();
        }
        TEXT_REQUEST = 0;


    }

    public void reiniciar(View view) {
        usuario.setText("");
        email.setText("");
        pwd.setText("");
        nombre.setText("");
        apellidos.setText("");
        fecha.setText("");
        sexo.setAdapter(adapter);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {}

    @Override
    public void onNothingSelected(AdapterView<?> parent) {}

    public void fecha(View view) {
        switch (view.getId()) {
            case R.id.etxtFecha:
                showDatePickerDialog();
                break;
        }
    }

    private void showDatePickerDialog() {
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.show(this.getSupportFragmentManager(), "datePicker");
    }

    public void processDatePickerResult(int year, int month, int day) {
        String month_string = Integer.toString(month+1);
        String day_string = Integer.toString(day);
        String year_string = Integer.toString(year);
        String dateMessage = (month_string +
                "/" + day_string + "/" + year_string);
        fecha.setText(dateMessage);
    }




    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }


        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            Registro activity = (Registro) getActivity();
            activity.processDatePickerResult(year,month,dayOfMonth);
        }


    }

    // CAMARA


    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_REQUEST).show();
        } else {
            String error = String.format("Error", errorReason.toString());
            Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==CODI_CAMERA && resultCode==RESULT_OK) {
            Bitmap bm = (Bitmap) data.getExtras().get("data");
            if (bm != null) {
                camara.setImageBitmap(bm);
            }
        }
        else if (requestCode == RECOVERY_REQUEST) {
            //getYouTubePlayerProvider().initialize("YOUR API KEY", this);
        }
    }
}

