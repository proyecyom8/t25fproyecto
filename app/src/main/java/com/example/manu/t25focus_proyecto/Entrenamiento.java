package com.example.manu.t25focus_proyecto;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerView;

public class Entrenamiento extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

    private static final int RECOVERY_REQUEST = 1;
    private TextView ejercicio;
    private YouTubePlayerView visualizar;
    private String message;
    private String propietario;
    private String videoURL;
    private Button empezar;
    private ImageView camara;
    final int CODI_CAMERA = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entrenamiento);
        Intent intent = getIntent();
        propietario = "";
        propietario = intent.getStringExtra(Menu.EXTRA_MESSAGE2);
        if(propietario == ""){
            propietario = intent.getStringExtra(ModificarEntrenamiento.EXTRA_MESSAGE2);
        }
        if(propietario == "menu"){
            message = intent.getStringExtra(Menu.EXTRA_MESSAGE);
        }
        else {
            message = intent.getStringExtra(ModificarEntrenamiento.EXTRA_MESSAGE);

        }
        camara = (ImageView) findViewById(R.id.camaraImg);

        ejercicio = findViewById(R.id.txt_titulo_ejercicio);
        visualizar = findViewById(R.id.video);
        empezar = findViewById(R.id.btn_empezar_entremaniento);

        if (message.equals("login3") || message.equals("6")) {
            ejercicio.setText("Cardio");
            videoURL = "YQ3WlNzWaUA"; // https://www.youtube.com/watch?v=YQ3WlNzWaUA

        } else if (message.equals("login4") || message.equals("7")) {
            ejercicio.setText("Upper Focus");
            videoURL = "MdWuNGPXElM"; // https://www.youtube.com/watch?v=MdWuNGPXElM

        } else {
            ejercicio.setText("AB Intervals");
            videoURL = "2dNImR5wQKk"; // https://www.youtube.com/watch?v=2dNImR5wQKk

        }

        camara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(i, CODI_CAMERA);
            }
        });
    }

    public void modificar_ejercicio(View view) {
        Intent intent = new Intent(this, ModificarEntrenamiento.class);
        startActivity(intent);
        finish();
    }

    public void empezar(View view) {
        visualizar.initialize("YOUR API KEY", this);
        empezar.setEnabled(false);

    }

    public void finalizar(View view) {
        dialogTiempoEntreno();

    }

    private void dialogTiempoEntreno() {
        AlertDialog.Builder VentanaTiempoEntreno =
                new AlertDialog.Builder(Entrenamiento.this);

        final CharSequence[] items = new CharSequence[4];

        items[0] = "0 - 5 min.";
        items[1] = "5 - 10 min.";
        items[2] = "10 - 15 min.";
        items[3] = "+ 15 min.";

        VentanaTiempoEntreno.setTitle(R.string.entrenado)
                .setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(
                                getApplicationContext(),
                                "Seleccionaste: " + items[which] + " Gracias",
                                Toast.LENGTH_SHORT)
                                .show();
                    }
                });
        dialogDificultadEntreno();
        VentanaTiempoEntreno.show();
    }

    private void dialogDificultadEntreno() {
        AlertDialog.Builder VentanaDificultadEntreno =
                new AlertDialog.Builder(Entrenamiento.this);


        VentanaDificultadEntreno.setTitle(R.string.dificultad);
        VentanaDificultadEntreno.setMessage(R.string.pregunta_dificultad);

        VentanaDificultadEntreno.setNegativeButton(R.string.opcion_dificil,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getApplicationContext(),
                                R.string.apretado_dificil,
                                Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });

        VentanaDificultadEntreno.setNeutralButton(R.string.opcion_normal,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getApplicationContext(),
                                R.string.apretado_normal,
                                Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });

        VentanaDificultadEntreno.setPositiveButton(R.string.opcion_facil,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getApplicationContext(),
                                R.string.apretado_facil,
                                Toast.LENGTH_SHORT).show();
                        finish();
                    }

                });
        VentanaDificultadEntreno.show();
    }

    @Override
    public void onInitializationSuccess(Provider provider, YouTubePlayer player, boolean wasRestored) {
        if (!wasRestored) {
            player.cueVideo(videoURL);
        }
    }

    @Override
    public void onInitializationFailure(Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_REQUEST).show();
        } else {
            String error = String.format("Error", errorReason.toString());
            Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==CODI_CAMERA && resultCode==RESULT_OK) {
            Bitmap bm = (Bitmap) data.getExtras().get("data");
            if (bm != null) {
                camara.setImageBitmap(bm);
            }
        }
        else if (requestCode == RECOVERY_REQUEST) {
            getYouTubePlayerProvider().initialize("YOUR API KEY", this);
        }
    }

    protected Provider getYouTubePlayerProvider() {
        return visualizar;
    }

}
