package com.example.manu.t25focus_proyecto;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;


@Entity(tableName = "user_table")
public class User {


    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "usernameUser")
    private String mNombreUser;


    @NonNull
    @ColumnInfo(name = "correo_user")
    private String mCorreo;

    @NonNull
    @ColumnInfo(name = "pass_user")
    private String mPassword;

    @NonNull
    @ColumnInfo(name = "name_user")
    private String mNombre;

    @NonNull
    @ColumnInfo(name = "apellido_user")
    private String mApellido;

    @NonNull
    @ColumnInfo(name = "sexo_user")
    private String mSexo;

    @NonNull
    @ColumnInfo(name = "fnacimiento_user")
    private String mFNacimiento;

    @NonNull
    @ColumnInfo(name = "peso")
    private String peso = "";

    @NonNull
    @ColumnInfo(name = "brazo")
    private String brazo = "";

    @NonNull
    @ColumnInfo(name = "pecho")
    private String pecho = "";

    @NonNull
    @ColumnInfo(name = "cintura")
    private String cintura = "";

    @NonNull
    @ColumnInfo(name = "piernas")
    private String piernas = "";



    public User(@NonNull String mNombreUser,@NonNull String mCorreo,@NonNull String mPassword,
                @NonNull String mNombre,@NonNull String mApellido,@NonNull String mSexo,
                @NonNull String mFNacimiento) {
        this.mNombreUser = mNombreUser;
        this.mCorreo = mCorreo;
        this.mPassword = mPassword;
        this.mNombre = mNombre;
        this.mApellido = mApellido;
        this.mSexo = mSexo;
        this.mFNacimiento = mFNacimiento;
    }


    @NonNull
    public String getMNombreUser() {
        return this.mNombreUser;
    }


    @NonNull
    public String getMCorreo() {
        return mCorreo;
    }

    @NonNull
    public String getMPassword() {
        return mPassword;
    }

    @NonNull
    public String getMNombre() {
        return mNombre;
    }

    @NonNull
    public String getMApellido() {
        return mApellido;
    }

    @NonNull
    public String getMSexo() {
        return mSexo;
    }

    @NonNull
    public String getMFNacimiento() {
        return mFNacimiento;
    }

    @NonNull
    public String getPeso() {
        return peso;
    }

    @NonNull
    public String getPecho() {
        return pecho;
    }

    @NonNull
    public String getCintura() {
        return cintura;
    }

    @NonNull
    public String getBrazo() {
        return brazo;
    }

    @NonNull
    public String getPiernas() {
        return piernas;
    }

    @NonNull
    public void setPeso(String peso) {  this.peso = peso;
    }

    @NonNull
    public void setPecho(String pecho) {  this.pecho = pecho;
    }

    @NonNull
    public void setBrazo(String brazo) {  this.brazo = brazo;
    }

    @NonNull
    public void setCintura(String cintura) {  this.cintura = cintura;
    }

    @NonNull
    public void setPiernas(String piernas) {  this.piernas = piernas;
    }



}