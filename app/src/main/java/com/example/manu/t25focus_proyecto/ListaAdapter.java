package com.example.manu.t25focus_proyecto;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import java.util.LinkedList;


public class ListaAdapter extends
        RecyclerView.Adapter<ListaAdapter.ViewHolder> {

    private final LinkedList<Integer> lista;
    private LayoutInflater mInflater;

    public ListaAdapter(Context context, LinkedList<Integer> lista) {
        mInflater = LayoutInflater.from(context);
        this.lista = lista;
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        public final ImageView imageItemViewAb, imageItemViewCardio, imageItemViewUpper, imageItemViewCons, imageItemViewCons2;
        final ListaAdapter mAdapter;

        public ViewHolder(View itemView, ListaAdapter adapter) {
            super(itemView);
            imageItemViewAb = itemView.findViewById(R.id.imagenAb);
            imageItemViewCardio = itemView.findViewById(R.id.imagenCardio);
            imageItemViewUpper = itemView.findViewById(R.id.imagenFocus);
            imageItemViewCons = itemView.findViewById(R.id.imagenCons);
            imageItemViewCons2 = itemView.findViewById(R.id.imagenCons2);
            this.mAdapter = adapter;
        }


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.lista_item, parent, false);
        return new ViewHolder(itemView, this);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {



    }

    @Override
    public int getItemCount() {
        return 1;
    }


}
