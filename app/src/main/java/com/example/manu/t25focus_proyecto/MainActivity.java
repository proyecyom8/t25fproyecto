package com.example.manu.t25focus_proyecto;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private EditText usuario, pwd;
    static List<User> usuarios = new ArrayList<User>();
    private UserViewModel mUserViewModel;
    private ImageView animacio;
    public static int TEXT_REQUEST = 0;
    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        usuario = findViewById(R.id.etxtUsuario);
        pwd = findViewById(R.id.etxtPassword);
        animacio = (ImageView) findViewById(R.id.animacioImg);


        mUserViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        mUserViewModel.getAllUsers().observe(this, new Observer<List<User>>() {
            @Override
            public void onChanged(@Nullable final List<User> users) {
                usuarios = users;


            }
        });


    }


    public void login(View view)  {
        boolean usuarioCorrecto = false;


        for (int i = 0; i < usuarios.size(); i++) {
            if (usuarios.get(i).getMNombreUser().equals(usuario.getText().toString())
                    && usuarios.get(i).getMPassword().equals(pwd.getText().toString())) {

                usuarioCorrecto = true;

                Menu.usuario = new String[]{usuarios.get(i).getMNombreUser(), usuarios.get(i).getMCorreo(),
                        usuarios.get(i).getMPassword(), usuarios.get(i).getMNombre(), usuarios.get(i).getMApellido(),
                        usuarios.get(i).getMSexo(), usuarios.get(i).getMFNacimiento(),usuarios.get(i).getPeso(),
                        usuarios.get(i).getBrazo(),usuarios.get(i).getPecho(),
                        usuarios.get(i).getCintura(),usuarios.get(i).getPiernas()};

                break;

            }
        }
        if (!usuarioCorrecto) {
            Toast.makeText(this, getString(R.string.usuarioContr_erroneos), Toast.LENGTH_LONG).show();
        } else {


            final AnimationDrawable frameAnimacio = (AnimationDrawable) animacio.getDrawable();
            animacio.setVisibility(View.VISIBLE);
            frameAnimacio.start();
            final Intent intent = new Intent(this, Menu.class);

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    frameAnimacio.stop();
                    animacio.setVisibility(View.INVISIBLE);
                    startActivity(intent);

                }
            },2000);

        }


    }

    public void registrar(View view) {
        Intent intent = new Intent(this, Registro.class);
        Registro.TEXT_REQUEST = 0;
        startActivity(intent);
        finish();

    }
}