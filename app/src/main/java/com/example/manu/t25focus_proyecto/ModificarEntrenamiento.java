package com.example.manu.t25focus_proyecto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.Calendar;
import java.util.LinkedList;

public class ModificarEntrenamiento extends AppCompatActivity {

    private final LinkedList<Integer> lista = new LinkedList<>();
    public static final String EXTRA_MESSAGE =
            "com.example.manu.t25focus_proyecto.extra.message";
    public static final String EXTRA_MESSAGE2 =
            "com.example.manu.t25focus_proyecto.extra.propietario";
    private String message;
    private final String propietario = "modificar";
    private RecyclerView mRecyclerview;
    private ListaAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar_entrenamiento);
        lista.add(R.drawable.abintervals);
        lista.add(R.drawable.cardio);
        lista.add(R.drawable.focusupper);
        lista.add(R.drawable.construccion);
        lista.add(R.drawable.construccion);
        mRecyclerview = findViewById(R.id.recyclerview);
        mAdapter = new ListaAdapter(this,lista);
        mRecyclerview.setAdapter(mAdapter);
        mRecyclerview.setLayoutManager(new LinearLayoutManager(this));

    }


    public void abIntervals(View view) {
        message = "5";
        Intent intent = new Intent(this, Entrenamiento.class);
        intent.putExtra(EXTRA_MESSAGE, message);
        intent.putExtra(EXTRA_MESSAGE2, propietario);
        startActivity(intent);
        finish();
    }

    public void cardio(View view) {
        message = "login3";
        Intent intent = new Intent(this, Entrenamiento.class);
        intent.putExtra(EXTRA_MESSAGE, message);
        intent.putExtra(EXTRA_MESSAGE2, propietario);
        startActivity(intent);
        finish();
    }

    public void upperFocus(View view) {
        message = "login4";
        Intent intent = new Intent(this, Entrenamiento.class);
        intent.putExtra(EXTRA_MESSAGE, message);
        intent.putExtra(EXTRA_MESSAGE2, propietario);
        startActivity(intent);
        finish();
    }
}
