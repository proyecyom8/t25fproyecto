package com.example.manu.t25focus_proyecto;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Medidas extends AppCompatActivity {

    private UserViewModel mUserViewModel;
    private Button aceptar, cancelar;
    private EditText peso,pecho,brazo,cintura,piernas;
    static String[] usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medidas);
        aceptar = findViewById(R.id.btn_aceptar);
        cancelar = findViewById(R.id.btn_cancelar);
        peso = findViewById(R.id.et_peso);
        pecho = findViewById(R.id.et_pecho);
        brazo = findViewById(R.id.et_brazos);
        cintura = findViewById(R.id.et_cintura);
        piernas = findViewById(R.id.et_piernas);
        usuario = Menu.usuario;

        peso.setText(usuario[7]);
        brazo.setText(usuario[8]);
        pecho.setText(usuario[9]);
        cintura.setText(usuario[10]);
        piernas.setText(usuario[11]);
        mUserViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
    }

    public void aceptarBTN (View view){
        User cuenta = new User (usuario[0],usuario[1],usuario[2],usuario[3],usuario[4],
                usuario[5],usuario[6]);
        cuenta.setPeso((peso.getText().toString()));
        cuenta.setBrazo(brazo.getText().toString());
        cuenta.setPecho(pecho.getText().toString());
        cuenta.setCintura(cintura.getText().toString());
        cuenta.setPiernas(piernas.getText().toString());
        mUserViewModel.update(cuenta);
        usuario[7] = peso.getText().toString();
        usuario[8] = brazo.getText().toString();
        usuario[9] = pecho.getText().toString();
        usuario[10] = cintura.getText().toString();
        usuario[11] = piernas.getText().toString();
        Toast.makeText(Medidas.this, "Has introducido las medidas!", Toast.LENGTH_LONG).show();
        finish();
    }
    public void cancelarBTN (View view){
        Toast.makeText(Medidas.this, "Has cancelado la introducción de medidas!", Toast.LENGTH_LONG).show();
        finish();
    }

    public void compartirBTN (View view){
        Toast.makeText(Medidas.this, "Vas a compartir tu progreso!", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, Compartir.class);
        startActivity(intent);
        finish();
    }
}
