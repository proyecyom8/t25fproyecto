package com.example.manu.t25focus_proyecto;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;

import java.util.Calendar;
import java.util.Date;

public class Menu extends AppCompatActivity {

    static  String[] usuario;
    private DatePicker calendario;

    public static final String EXTRA_MESSAGE =
            "com.example.manu.t25focus_proyecto.extra.message";
    public static final String EXTRA_MESSAGE2 =
            "com.example.manu.t25focus_proyecto.extra.propietario";
    private String message;
    private final String propietario = "menu";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        calendario = findViewById(R.id.calendar);
        MainActivity.TEXT_REQUEST = 1;

    }

    public void dia_seleccionado(View view) {
        Date fecha;
        fecha = new Date(calendario.getYear(),calendario.getMonth(),calendario.getDayOfMonth());
        Calendar cal = Calendar.getInstance();
        cal.setTime(fecha);
        message = Integer.toString(cal.get(Calendar.DAY_OF_WEEK));
        Intent intent = new Intent(this, Entrenamiento.class);
        intent.putExtra(EXTRA_MESSAGE, message);
        intent.putExtra(EXTRA_MESSAGE2, propietario);
        startActivity(intent);




    }

    public void introducirMedidas (View view){
        Intent intent = new Intent(this, Medidas.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.actionPerfil:
                Intent intent = new Intent(this, Registro.class);
                intent.putExtra("usuario",usuario);
                Registro.TEXT_REQUEST = 1;
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
