package com.example.manu.t25focus_proyecto;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class Compartir extends AppCompatActivity {


    //Identificadors dels botons del menú
    private final int[] BUTTONS={R.id.facebook, R.id.twitter, R.id.instagram};

    //Vector que contindrà els objectes fragments
    Fragment[] fragments = new Fragment[3];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compartir);
        inicialitzarFragments();
        inicialitzarMenu();
    }

    private void inicialitzarFragments(){

        fragments[0] = new Facebook();
        fragments[1] = new Twitter();
        fragments[2] = new Instagram();
    }

    private void inicialitzarMenu(){
        ImageButton button;
        for (int i=0; i<BUTTONS.length; i++) {
            button = (ImageButton) findViewById(BUTTONS[i]);
            final int btn = i;
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                /**
                 * Mètode que tracta el event onClick dels botons del menú.
                 * Es carregarà en el contenidor de l'activity main el fragment que s'hagi premut.
                 */
                public void onClick(View v) {
                    FragmentManager manager = getSupportFragmentManager();
                    FragmentTransaction transaction = manager.beginTransaction();
                    transaction.replace(R.id.container, fragments[btn]);
                    transaction.commit();
                }
            });
        }
    }
}
